package main

import (
	"log"

	config "./config"
	controllers "./controllers"
	db "./db"
	"github.com/buaazp/fasthttprouter"
	_ "github.com/lib/pq"
	"github.com/valyala/fasthttp"
)

func main() {
	connection, _ := db.ConnectSQL(config.USER, config.HOST, config.PORT, config.NAME)
	controllers.DataBase = connection.SQL
	router := fasthttprouter.New()
	router.GET("/host", controllers.GetHosts)
	router.GET("/host/:host", controllers.GetHost)
	log.Fatal(fasthttp.ListenAndServe(":80", router.Handler))
}
