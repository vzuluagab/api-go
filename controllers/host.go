package controllers

import (
	"database/sql"
	"encoding/json"
	"time"

	config "../config"
	models "../models"
	"github.com/valyala/fasthttp"
)

var DataBase *sql.DB

/**
 * Returns information related to a host
 * @param [RequestCtx] ctx- Request
 */
func GetHost(ctx *fasthttp.RequestCtx) {
	statusCode := 200
	hostName, _ := ctx.UserValue("host").(string)
	hostStruct := models.Host{}
	previousHost := models.GetHost(DataBase, hostName)
	status, responseBody, errorRequest := fasthttp.GetTimeout(nil, config.API_URL_ANALYZE+hostName+config.PARAMS_URL, time.Duration(time.Second*config.TIMEOUT))
	if errorRequest != nil {
		statusCode = 500
		return
	}
	if status != fasthttp.StatusOK {
		statusCode = status
		return
	}
	json.Unmarshal(responseBody, &hostStruct)
	if hostStruct.Status == "ERROR" {
		statusCode = 404
		hostStruct.IsDown = true
	}
	WhoIs(hostStruct.Endpoints)
	hostStruct.PreviousSslGrade = previousHost.SslGrade
	hostStruct.ServersChanged = ServersChanged(previousHost.Endpoints, hostStruct.Endpoints)
	hostStruct.SetTitleLogo(hostName)
	SetSslGrade(&hostStruct)
	hostStruct.Create(DataBase)
	Response(ctx, &hostStruct, statusCode)
}

/**
 * Returns the information of recently consulted hosts
 * @param [RequestCtx] ctx- Request
 */
func GetHosts(ctx *fasthttp.RequestCtx) {
	statusCode := 200
	hostsStructs := models.GetHosts(DataBase)
	Response(ctx, hostsStructs, statusCode)
}

/**
 * Write the response of a request
 * @param [RequestCtx] ctx- Request
 * @param [Host] responseData- Information to return
 * @param [int] statusCode- Request
 */
func Response(ctx *fasthttp.RequestCtx, responseData interface{}, statusCode int) {
	response, _ := json.Marshal(&responseData)
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Content-Type", "application/json; charset=utf-8")
	ctx.Response.Header.Set("Access-Control-Allow-Credentials", "true")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
	ctx.SetStatusCode(statusCode)
	ctx.Write(response)
}
