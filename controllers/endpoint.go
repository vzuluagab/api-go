package controllers

import (
	"reflect"
	"regexp"
	"strings"

	models "../models"
	"github.com/likexian/whois-go"
)

/**
 * Check the country and the owners of each of the servers
 * @param [slice] serversArray- servers to which country and owner must be assigned
 */
func WhoIs(serversArray []*models.Endpoint) {
	if len(serversArray) == 0 {
		return
	}
	for _, server := range serversArray {
		dataWhoIs, errorWhoIs := whois.Whois(server.IpAddress)
		if errorWhoIs != nil {
			continue
		}
		arrayWhoIs := strings.Split(dataWhoIs, "\n")
		server.Country = GetData(arrayWhoIs, "country")
		server.Owner = GetData(arrayWhoIs, "Owner|OrgName")
	}
}

/**
 * Split a string based on a word
 * @param [string] dataWhoIs - string obtained when running the whois command
 * @param [string] attribute - word by which you want to divide the string
 */
func GetData(dataWhoIs []string, attribute string) string {
	re := regexp.MustCompile("(?i)" + attribute)
	for _, line := range dataWhoIs {
		if re.MatchString(line) {
			linePart := strings.Split(line, ": ")
			return strings.TrimSpace(linePart[1])
		}
	}

	return ""
}

/**
 * Determines which is the lowest rank of a list of servers
 * @param [Host] host
 */
func SetSslGrade(host *models.Host) {
	if len(host.Endpoints) == 0 {
		return
	}
	arrayGrades := map[string]int{"A+": 6, "A": 5, "B": 4, "C": 3, "D": 2, "E": 1, "F": 0}
	valueMinGrade := arrayGrades["A+"]
	minGrade := "A+"
	for _, server := range host.Endpoints {
		if server.SslGrade == "" {
			host.SslGrade = ""
			continue
		}
		if arrayGrades[server.SslGrade] < valueMinGrade {
			valueMinGrade = arrayGrades[server.SslGrade]
			minGrade = server.SslGrade
		}
	}
	host.SslGrade = minGrade
}

/**
 * Compare two server listings and determine if they are the same
 * @param [slice] serversPrevious- servers associated with the host in a previous query
 * @param [slice] serversHost- servers currently associated with the host
 * @return bool
 */
func ServersChanged(serversPrevious []*models.Endpoint, serversHost []*models.Endpoint) bool {
	if len(serversPrevious) == 0 {
		return false
	}
	if len(serversPrevious) != len(serversHost) {
		return true
	}
	for index, server := range serversHost {
		if !reflect.DeepEqual(server, serversPrevious[index]) {
			return true
		}
	}
	return false
}
