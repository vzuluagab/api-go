package models

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"github.com/gocolly/colly"
)

//Represents the consulted host
type Host struct {
	Host             string      `json:"host"`
	SslGrade         string      `json:"sslGrade"`
	Logo             string      `json:"logo"`
	Title            string      `json:"title"`
	Status           string      `json:"status"`
	StatusMessage    string      `json:"statusMessage"`
	ServersChanged   bool        `json:"serversChanged"`
	PreviousSslGrade string      `json:"previousSslGrade"`
	IsDown           bool        `json:"isDown"`
	Endpoints        []*Endpoint `json:"endpoints"`
}

/**
 * Consult the title and image of a web page
 * @param [string] hostName - name of the website from which the information should be extracted
 */
func (this *Host) SetTitleLogo(hostName string) {
	collector := colly.NewCollector()
	collector.OnHTML("head", func(htmlElement *colly.HTMLElement) {
		this.Title = htmlElement.ChildText("title")
		this.Logo = htmlElement.ChildAttr(`link[rel="shortcut icon"]`, "href")
	})
	collector.Limit(&colly.LimitRule{
		RandomDelay: 1 * time.Second,
	})
	collector.Visit("https://" + hostName)
}

/**
 * Inserts the host and servers into the database
 * @param [Struct] database
 */
func (this *Host) Create(connDatabase *sql.DB) {
	isDown := strconv.FormatBool(this.IsDown)
	sql := `INSERT INTO host(host_name, ssl_grade, previous_ssl_grade,
		logo, title, is_down, is_down_message, query_at)
	VALUES ('` + this.Host + `', '` + this.SslGrade + `', '` + this.PreviousSslGrade + `',
	 '` + this.Logo + `', '` + this.Title + `', '` + isDown + `', '` + this.StatusMessage + `', NOW())
	ON CONFLICT (host_name)
	DO UPDATE SET ssl_grade='` + this.SslGrade + `', previous_ssl_grade='` + this.PreviousSslGrade + `',
	logo='` + this.Logo + `', title='` + this.Title + `', is_down='` + isDown + `',
	is_down_message='` + this.StatusMessage + `', query_at=NOW();`
	_, errorExecute := connDatabase.Exec(sql)
	if errorExecute != nil {
		return
	}
	CreateServers(connDatabase, this.Endpoints, this.Host)
}

/**
 * Returns the queried host
 * @param [Struct] database
 * @param [string] hostName
 * @return Host
 */
func GetHost(connDatabase *sql.DB, hostName string) Host {
	var previousHost = Host{}
	query := `SELECT host.host_name, host.ssl_grade, host.logo,
	host.title, COALESCE(server.ip_address, ''), COALESCE(server.server_name, ''),
	COALESCE(server.grade, ''), COALESCE(server.country, ''), COALESCE(server.owner, '') 
	FROM host
	LEFT JOIN server ON host.host_name = server.host_name
	WHERE host.host_name = $1;`
	result, _ := connDatabase.Query(query, hostName)
	for result.Next() {
		var serverHost = Endpoint{}
		err := result.Scan(
			&previousHost.Host,
			&previousHost.SslGrade,
			&previousHost.Logo,
			&previousHost.Title,
			&serverHost.IpAddress,
			&serverHost.Name,
			&serverHost.SslGrade,
			&serverHost.Country,
			&serverHost.Owner,
		)
		if err != nil {
			fmt.Println(query)
			panic(err.Error())
		}
		previousHost.Endpoints = append(previousHost.Endpoints, &serverHost)
	}

	return previousHost
}

/**
 * Returns recently consulted hosts
 * @param [Struct] database
 * @return slice
 */
func GetHosts(connDatabase *sql.DB) []*Host {
	var items []*Host
	sql := `SELECT host.host_name, COALESCE(host.ssl_grade, ''), COALESCE(host.previous_ssl_grade, ''), 
	COALESCE(host.logo, ''), COALESCE(host.title, ''), is_down, COALESCE(host.is_down_message, '') 
	FROM host
	ORDER BY query_at DESC
	LIMIT 10;`
	results, err := connDatabase.Query(sql)
	if err != nil {
		fmt.Println(err.Error())
		return items
	}
	for results.Next() {
		var host = Host{}
		err := results.Scan(
			&host.Host,
			&host.SslGrade,
			&host.PreviousSslGrade,
			&host.Logo,
			&host.Title,
			&host.IsDown,
			&host.StatusMessage,
		)
		if err != nil {
			fmt.Println(sql)
			panic(err.Error())
		}
		if err != nil {
			return items
		}
		host.Endpoints = GetServers(connDatabase, host.Host)
		items = append(items, &host)
	}

	return items
}
