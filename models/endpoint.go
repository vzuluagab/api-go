package models

import (
	"database/sql"
	"fmt"
	"strings"
)

//Represents a server
type Endpoint struct {
	IpAddress string `json:"ipAddress"`
	Name      string `json:"serverName"`
	SslGrade  string `json:"grade"`
	Country   string `json:"country"`
	Owner     string `json:"owner"`
}

/**
 * Insert servers related to a host
 * @param [Struct] connDatabase
 * @param [slice] arrayServers
 * @param [string] hostName
 * @return slice
 */
func CreateServers(connDatabase *sql.DB, arrayServers []*Endpoint, hostName string) {
	if len(arrayServers) == 0 {
		return
	}
	rows := make([]string, 0)
	sql := `INSERT INTO server(ip_address, host_name, server_name, grade, country, owner) 
	VALUES %s`
	for _, server := range arrayServers {
		row := fmt.Sprintf("('%s', '%s', '%s', '%s', '%s', '%s')",
			server.IpAddress,
			hostName,
			server.Name,
			server.SslGrade,
			server.Country,
			server.Owner,
		)
		rows = append(rows, row)
	}
	sql = fmt.Sprintf(sql, strings.Join(rows, ","))
	DeleteServers(connDatabase, hostName)
	_, errorExecute := connDatabase.Exec(sql)
	if errorExecute != nil {
		panic(errorExecute.Error())
	}
}

/**
 * Remove servers related to a host
 * @param [Struct] database
 * @param [string] hostName
 * @return slice
 */
func GetServers(connDatabase *sql.DB, hostName string) []*Endpoint {
	var endpoints = []*Endpoint{}
	query := `SELECT server.ip_address, server.server_name,
	server.grade, server.country, server.owner 
	FROM server
	WHERE host_name = $1
	ORDER BY ip_address DESC;`
	result, _ := connDatabase.Query(query, hostName)
	for result.Next() {
		var endpoint = Endpoint{}
		err := result.Scan(
			&endpoint.IpAddress,
			&endpoint.Name,
			&endpoint.SslGrade,
			&endpoint.Country,
			&endpoint.Owner,
		)
		if err != nil {
			fmt.Println(query)
			panic(err.Error())
		}
		endpoints = append(endpoints, &endpoint)
	}

	return endpoints
}

/**
 * Remove servers related to a domain
 * @param [Struct] database
 * @param [string] hostName
 */
func DeleteServers(connDatabase *sql.DB, hostName string) {
	sql := `DELETE FROM server WHERE host_name='%s'`
	sql = fmt.Sprintf(sql, hostName)
	_, errorExecute := connDatabase.Exec(sql)
	if errorExecute != nil {
		panic(errorExecute.Error())
	}
}
