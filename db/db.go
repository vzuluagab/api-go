package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type DB struct {
	SQL *sql.DB
}

var dbConn = &DB{}

func ConnectSQL(user, host, port, name string) (*DB, error) {
	dbSource := fmt.Sprintf(
		"postgresql://%s@%s:%s/%s?sslmode=disable",
		user,
		host,
		port,
		name,
	)
	d, err := sql.Open("postgres", dbSource)
	if err != nil {
		panic(err)
	}
	dbConn.SQL = d
	return dbConn, err
}
