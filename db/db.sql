CREATE DATABASE IF NOT EXISTS hosts;
USE hosts;
-- DROP TABLE server;
-- DROP TABLE host;

CREATE TABLE IF NOT EXISTS host (
   host_name VARCHAR(250) NOT NULL PRIMARY KEY,
   ssl_grade VARCHAR(2) NULL,
   logo VARCHAR(500) NULL,
   title VARCHAR(250) NULL,
   query_at TIMESTAMP
  );

ALTER TABLE host 
ADD COLUMN previous_ssl_grade VARCHAR(2) NULL;

ALTER TABLE host 
ADD COLUMN is_down BOOL DEFAULT FALSE NULL;

ALTER TABLE host 
ADD COLUMN is_down_message VARCHAR(500) NULL;

CREATE TABLE IF NOT EXISTS server (
   ip_address VARCHAR(250) NOT NULL,
   host_name VARCHAR(250) NOT NULL,
   server_name VARCHAR(250) NULL,
   grade VARCHAR(2) NULL,
   country VARCHAR(100) NULL,
   owner VARCHAR(250) NULL,
   PRIMARY KEY(ip_address, host_name),
   CONSTRAINT server_host_fkey FOREIGN KEY (host_name) REFERENCES host (host_name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE INDEX query_at_index ON host (query_at);

-- SET SQL_SAFE_UPDATES=false;
-- ALTER TABLE host DROP COLUMN status;
-- ALTER TABLE host DROP COLUMN status_message;
