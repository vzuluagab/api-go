# Changelog

Todos los cambios notables del proyecto se documentarán en este archivo.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [1.0.0] - 2020-06-22

### Added

- API REST /host/:host para consultar información de un dominio
- API REST para obtener dominios consultados

## [1.0.X] - 2020-12-08

### Fixed

- Ajuste consulta dominios /host