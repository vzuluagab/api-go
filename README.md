# API Servidores

API que permite consultar un dominio y la configuración de sus servidores.

## Comenzando

Estas intrucciones permiten obtener una copia del proyecto para desarrollo y pruebas

- Clonar repositorio: `https://gitlab.com/vzuluagab/api-go`

### Prerequisitos

- Instalar Golang
- Instalar motor de base de datos [CockroachDB](https://www.cockroachlabs.com/docs/stable/install-cockroachdb-windows.html)
- Instalar dependencias necesarias:
  - `go get -u github.com/lib/pq`
  - `go get -u github.com/gocolly/colly`
  - `go get -u github.com/likexian/whois-go`
  - `go get -u github.com/valyala/fasthttp`
  - `go get -u github.com/buaazp/fasthttprouter`
- Inicializar nodo en CockroachDB `cockroach start-single-node --insecure`
- Habilitar ejecución sql en CockroachDB `cockroach sql --insecure --host=localhost --user=root` y ejecutar script sql disponible en db/db.sql
